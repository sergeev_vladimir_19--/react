import React ,{useState}from "react";
import MyInput from "./UI/Button/input/MyInput";
import MyButton from "./UI/Button/MyButton";
const PostForm=(create)=>{
    const[post,setPost]=useState({title:'',body:''})
    const addNewPost=(e)=>{
        e.preventDefault()
        const newPost={
            ...post,id:Date.now()
        }
        create(newPost)
        setPost({title:'',body:''}) 
      }
 
    return(
        <div>
            <form>
                <MyInput value={post.title} onChange={e=>setPost({...post,title:e.target.value})} type="text" placeholder=" Введите автора"/>
                <MyInput  value={post.body} onChange={e=>setPost({...post,body:e.target.value})}type="text" placeholder=" название книги"/>
                <MyButton onClick={addNewPost}>Создать</MyButton>
             </form>

        </div>
    )
}
export default PostForm