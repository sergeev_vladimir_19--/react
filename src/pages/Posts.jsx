import React, { useRef, useState } from "react";

import PostList from "./components/PostList";


import './styles/App.css'
import PostForm from "./components/PostForm";

function Posts() {
  const [posts,setPosts]=useState([
    {id:1,value:"Книга№"},
    {id:4,value:"Книга№"},
    {id:7,value:"Книга№"},
  ])
  const createPost=(newPost)=>{
    setPosts([...posts,newPost])
  }


  return (
    <div className="App">
      <PostForm create={createPost}/>
      <PostList posts={posts}/>
    </div>
  );
}

export default Posts;
